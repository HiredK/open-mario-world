package com.hiredk.omw.engine.utility 
{
	import flash.display.BitmapData;
	import flash.display.Bitmap;
	
	import flash.geom.Rectangle;
	import flash.geom.Matrix;
	import flash.geom.Point;
	
	// engine imports
	import com.hiredk.omw.engine.entity.Visual;
	
	/**
	 * @author HiredK
	 */
	public class SpriteSheet extends Bitmap {
		/*/////////////////////////////////////////////////
		/// MEMBERS:
		/////////////////////////////////////////////////*/
		private var m_name:String;
		private var m_wpxl:uint;
		private var m_hpxl:uint;
		private var m_freq:uint;
		private var m_step:uint;
		
		/*/////////////////////////////////////////////////
		/// CONSTRUCTOR:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	data
		 */
		public function SpriteSheet(name:String, data:BitmapData)
		{
			m_name = name;
			m_wpxl = 32;
			m_hpxl = 32;
			m_freq = 50;
			m_step = 1;
			
			super(data);
		}
		
		/*/////////////////////////////////////////////////
		/// SERVICES:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	dest
		 * @param	step
		 */
		public function BlitTo(dest:BitmapData, step:uint, side:uint):void {
			var srect:Rectangle = new Rectangle(m_wpxl * step, 0, m_wpxl, m_hpxl);
			if (side == Visual.NEGATIVE_SIDE)
			{
				var flip:BitmapData = new BitmapData(this.bitmapData.width, this.bitmapData.height, true, 0);
				flip.draw(this.bitmapData, new Matrix(-1, 0, 0, 1, this.bitmapData.width));
				dest.copyPixels(flip, srect, new Point(0, 0));
				return;
			}
			
			dest.copyPixels(this.bitmapData, srect, new Point(0, 0));
		}
		
		/**
		 * 
		 * @return
		 */
		public function Clone():SpriteSheet {
			var object:SpriteSheet = new SpriteSheet(m_name, this.bitmapData);
			object.PixelW = m_wpxl;
			object.PixelH = m_hpxl;
			object.Freq = m_freq;
			object.Step = m_step;
			return object;
		}
		
		/*/////////////////////////////////////////////////
		/// ACCESSORS:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 */
		public function set PixelW(value:uint):void {
			m_wpxl = value;
		}
		public function get PixelW():uint {
			return m_wpxl;
		}
		
		/**
		 * 
		 */
		public function set PixelH(value:uint):void {
			m_hpxl = value;
		}	
		public function get PixelH():uint {
			return m_hpxl;
		}
		
		/**
		 * 
		 */
		public function set Freq(value:uint):void {
			m_freq = value;
		}
		public function get Freq():uint {
			return m_freq;
		}
		
		/**
		 * 
		 */
		public function set Step(value:uint):void {
			m_step = value;
		}
		public function get Step():uint {
			return m_step;
		}
		
		/**
		 * 
		 */
		public function get Name():String {
			return m_name;
		}
	}
}