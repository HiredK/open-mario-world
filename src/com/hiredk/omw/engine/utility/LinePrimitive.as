package com.hiredk.omw.engine.utility 
{
	/**
	 * @author HiredK
	 */
	public class LinePrimitive {
		/*/////////////////////////////////////////////////
		/// MEMBERS:
		/////////////////////////////////////////////////*/
		private var m_ax:int;
		private var m_ay:int;
		private var m_bx:int;
		private var m_by:int;
		
		/*/////////////////////////////////////////////////
		/// CONSTRUCTOR:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	ax
		 * @param	ay
		 * @param	bx
		 * @param	by
		 */
		public function LinePrimitive(ax:int = 0, ay:int = 0, bx:int = 0, by:int = 0)
		{
			m_ax = ax;
			m_ay = ay;
			m_bx = bx;
			m_by = by;
		}
		
		/*/////////////////////////////////////////////////
		/// SERVICES:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	other
		 * @return
		 */
		public function Same(other:LinePrimitive):Boolean {
			return (m_ax == other.ax && m_ay == other.ay && m_bx == other.bx && m_by == other.by);
		}
		
		/**
		 * 
		 * @return
		 */
		public function IsXAligned():Boolean {
			return (m_ay == m_by);
		}
		
		/**
		 * 
		 * @return
		 */
		public function IsYAligned():Boolean {
			return (m_ax == m_bx);
		}
		
		/*/////////////////////////////////////////////////
		/// ACCESSORS:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 */
		public function set ax(value:int):void {
			m_ax = value;
		}
		public function get ax():int {
			return m_ax;
		}
		
		/**
		 * 
		 */
		public function set ay(value:int):void {
			m_ay = value;
		}
		public function get ay():int {
			return m_ay;
		}
		
		/**
		 * 
		 */
		public function set bx(value:int):void {
			m_bx = value;
		}
		public function get bx():int {
			return m_bx;
		}
		
		/**
		 * 
		 */
		public function set by(value:int):void {
			m_by = value;
		}
		public function get by():int {
			return m_by;
		}
	}
}