package com.hiredk.omw
{
	import flash.display.Sprite;
	
	/**
	 * @author HiredK
	 */
	public class Main extends Sprite {
		/*/////////////////////////////////////////////////
		/// ENTRY POINT:
		/////////////////////////////////////////////////*/
		/**
		 *
		 */
		public function Main():void
		{
			addChild(new Game);
		}
	}
}