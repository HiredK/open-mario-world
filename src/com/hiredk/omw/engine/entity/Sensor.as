package com.hiredk.omw.engine.entity 
{
	import flash.events.Event;
	
	// engine imports
	import com.deadreckoned.assetmanager.AssetManager;
	import com.hiredk.omw.engine.Sector;
	
	// JSON imports (as3corlib.swc)
	import com.adobe.serialization.json.JSON;
	
	// Box2D imports
	import Box2D.Dynamics.*;
	import Box2D.Collision.*;
	import Box2D.Collision.Shapes.*;
	import Box2D.Common.Math.*;
	
	import Box2D.Dynamics.Contacts.b2Contact;
	
	/**
	 * @author HiredK
	 */
	public class Sensor extends Physic {
		/*/////////////////////////////////////////////////
		/// MEMBERS:
		/////////////////////////////////////////////////*/
		protected var m_physicFixture:b2Fixture;
		protected var m_sensorFixture:b2Fixture;
		
		// ground test
		protected var m_groundedCheck:Boolean;
		protected var m_grounded:Boolean;
		
		/*/////////////////////////////////////////////////
		/// CONSTRUCTOR:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	parent
		 * @param	name
		 * @param	x
		 * @param	y
		 */
		public function Sensor(parent:Sector, name:String, x:int, y:int)
		{
			super(parent, name, x, y);
		}
		
		/*/////////////////////////////////////////////////
		/// SERVICES:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	w
		 * @param	h
		 * @param	category
		 */
		public function BuildSensor(w:uint, h:uint, f1:uint, f2:uint):void {
			var filter1:b2FilterData = new b2FilterData();
			filter1.categoryBits = f1;
				
			m_physicFixture = AddSquareFixture(0, 0, w, h, 1.0);
			m_physicFixture.SetFilterData(filter1);
			
			var filter2:b2FilterData = new b2FilterData();
			filter2.categoryBits = f2;
			
			m_sensorFixture = AddCircleFixture(0, w, w * 0.5, 0.0);
			m_sensorFixture.SetFilterData(filter2);
			
			m_body.SetSleepingAllowed(false);
			m_body.SetFixedRotation(true);
		}
		
		/**
		 * 
		 * @param	contact
		 */
		override public function CheckContact(contact:b2Contact):void
		{
			if ((contact.GetFixtureA() == m_sensorFixture ||
				 contact.GetFixtureB() == m_sensorFixture) &&
				 contact.IsTouching())
			{
				m_groundedCheck = true;
			}
		}
		
		/**
		 * 
		 * @param	e
		 */
		override public function Update(e:Event):void {
			m_grounded = (m_groundedCheck) ? true : false;
			m_groundedCheck = false;
			super.Update(e);
		}
		
		/**
		 * 
		 */
		override public function Reset():void {
			super.Reset();
		}
		
		/*/////////////////////////////////////////////////
		/// ACCESSORS:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @return
		 */
		public function isTouchingGround():Boolean {
			return m_grounded && Math.abs(m_body.GetLinearVelocity().y) < 5.0;
		}
	}
}