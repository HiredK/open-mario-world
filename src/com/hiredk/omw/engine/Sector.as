package com.hiredk.omw.engine 
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	import flash.display.BitmapData;
	import flash.display.Bitmap;
	
	import flash.geom.Rectangle;
	import flash.geom.Point;
	
	import flash.net.FileReference;
	
	// engine imports
	import com.deadreckoned.assetmanager.AssetManager;
	import com.hiredk.omw.engine.utility.LinePrimitive;
	import com.hiredk.omw.engine.utility.TilePattern;
	
	// entity imports
	import com.hiredk.omw.engine.entity.IEntity;
	import com.hiredk.omw.engine.entity.Visual;
	import com.hiredk.omw.engine.entity.Physic;
	import com.hiredk.omw.engine.entity.Player;
	import com.hiredk.omw.engine.entity.Turtle;
	
	// JSON imports (as3corlib.swc)
	import com.adobe.serialization.json.JSON;
	
	// Box2D imports
	import Box2D.Dynamics.*;
	import Box2D.Collision.*;
	import Box2D.Collision.Shapes.*;
	import Box2D.Common.Math.*;
	
	import Box2D.Dynamics.Contacts.b2Contact;
	
	/**
	 * @author HiredK
	 */
	public class Sector extends Sprite {
		/*/////////////////////////////////////////////////
		/// MEMBERS:
		/////////////////////////////////////////////////*/
		private var m_tilelist:Vector.<TilePattern>;
		private var m_entities:Vector.<IEntity>;
		private var m_body:b2Body;
		
		private var m_linelist:Vector.<LinePrimitive>;
		private var m_builded:Boolean;
		private var m_absX:int;
		private var m_absY:int;
		
		private var m_numX:int;
		private var m_numY:int;
		
		/*/////////////////////////////////////////////////
		/// CONSTRUCTOR:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	path
		 * @param	x
		 * @param	y
		 */
		public function Sector(path:String, x:int, y:int)
		{
			m_linelist = new Vector.<LinePrimitive>;
			m_tilelist = new Vector.<TilePattern>;
			m_entities = new Vector.<IEntity>;
			
			var bd:b2BodyDef = new b2BodyDef;
			bd.type = b2Body.b2_staticBody;
			
			bd.position.x = (m_absX = (this.x = x));
			bd.position.y = (m_absY = (this.y = y));
			
			m_body = World.GetDynamicWorld().CreateBody(bd);
			
			if (AssetManager.getInstance().contains(path)) {
				var info:Object = JSON.decode(AssetManager.getInstance().get(path).asset);
				if (info && BuildTerrain(info))
				{
					var array:Array = info["entities"];
					for (var i:uint = 0; i < array.length; i++) {
						switch(array[i].type)
						{
							case IEntity.VISUAL_ENTITY: {
								var visual:Visual = new Visual(this, array[i].name, array[i].xpos, array[i].ypos);
								if (visual.BuildSpriteList(array[i].path))
								{
									visual.SetAnimation(array[i].anim);
									visual.OffsetX = array[i].xoff;
									visual.OffsetY = array[i].yoff;
								}
								
								AddEntity(visual);
							} break;
							
							case IEntity.PHYSIC_ENTITY: {
								var physic:Physic = new Physic(this, array[i].name, array[i].xpos, array[i].ypos);
								if (physic.BuildSpriteList(array[i].path))
								{
									physic.SetAnimation(array[i].anim);
									physic.OffsetX = array[i].xoff;
									physic.OffsetY = array[i].yoff;
								}
								
								switch(array[i].body)
								{
									case "square": {
										physic.AddSquareFixture(0, 0, array[i].wdim, array[i].hdim, 1.0);
									} break;
										
									default:
									{
									} break;
								}
								
								AddEntity(physic);
							} break;
							
							case IEntity.TURTLE_ENTITY: {
								var turtle:Turtle = new Turtle(this, array[i].name, array[i].xpos, array[i].ypos);
								if (turtle.BuildSpriteList(array[i].path))
								{
									turtle.BuildSensor(array[i].wdim, array[i].hdim, Physic.CATEGORY_TURTLE_T, Physic.CATEGORY_TURTLE_B);
									turtle.OffsetX = array[i].xoff;
									turtle.OffsetY = array[i].yoff;
								}
								
								AddEntity(turtle);
							} break;
							
							default:
							{
							} break;
						}
					}
				}
			}
		}
		
		/*/////////////////////////////////////////////////
		/// SERVICES:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	path
		 * @return
		 */
		private function BuildTileset(path:String):Vector.<TilePattern> {
			var tileset:Vector.<TilePattern> = new Vector.<TilePattern>;
			if (AssetManager.getInstance().contains(path))
			{
				var info:Object = JSON.decode(AssetManager.getInstance().get(path).asset);
				if (info)
				{
					if (AssetManager.getInstance().contains(info.path)) {
						var source:BitmapData = AssetManager.getInstance().get(info.path).asset;
						if (source && (source.width % World.DEFAULT_TILE_SIZE) == 0 && (source.height % World.DEFAULT_TILE_SIZE) == 0) {
							var srect:Rectangle = new Rectangle(0, 0, World.DEFAULT_TILE_SIZE, World.DEFAULT_TILE_SIZE);
							var array:Array = info["data"];
							var index:uint = 1;
							
							for (var j:uint = 0; j < source.height / 16; j++) {
								for (var i:uint = 0; i < source.width / 16; i++) {
									var data:BitmapData = new BitmapData(World.DEFAULT_TILE_SIZE, World.DEFAULT_TILE_SIZE, true, 0xFFFFFF);
									data.copyPixels(source, srect, new Point(0, 0));
									
									var tile:TilePattern = new TilePattern(data);
									tile.Indice = index;
									tile.Ground = array[index - 1].ground;
									tileset.push(tile);
									
									srect.x += World.DEFAULT_TILE_SIZE;
									index++;
								}
								
								srect.y += World.DEFAULT_TILE_SIZE;
								srect.x = 0;
							}
						}
					}
				}
			}
			
			return tileset;
		}
		
		/**
		 * 
		 * @param	lines
		 * @return
		 */
		private function RemoveDuplicateLines(lines:Vector.<LinePrimitive>):Vector.<LinePrimitive> {
			var result:Vector.<LinePrimitive> = new Vector.<LinePrimitive>;
			for (var i:uint = 0; i < lines.length; i++)
			{
				var duplicate:Boolean = false;
				for (var j:uint = 0; j < lines.length; j++) {
					if (i != j  && lines[i].Same(lines[j])) {
						duplicate = true;
					}
				}
				
				if (!duplicate) {
					result.push(lines[i]);
				}
			}
			
			return result;
		}
		
		/**
		 * 
		 * @param	lines
		 * @param	xnum
		 * @param	ynum
		 */
		private function GenerateXLines(lines:Vector.<LinePrimitive>, xnum:uint, ynum:uint):Vector.<LinePrimitive> {
			var ordered:Vector.<LinePrimitive> = new Vector.<LinePrimitive>;
			for (var ty:uint = 0; ty < ynum; ty++)
			for (var tx:uint = 0; tx < xnum; tx++)
			{
				var n:uint = 0;
				while (n != lines.length) {
					if (lines[n].ax == tx * World.DEFAULT_TILE_SIZE &&
						lines[n].ay == ty * World.DEFAULT_TILE_SIZE &&
						lines[n].IsXAligned()) {
						ordered.push(lines[n]);
						break;
					} n++;
				}
			}
			
			var result:Vector.<LinePrimitive> = new Vector.<LinePrimitive>;
			var setX:Boolean = false;
			var minX:int = 0;
			var maxX:int = 0;
			
			for (var i:uint = 0; i < ordered.length; i++)
			{
				if (!setX) {
					minX = ordered[i].ax;
					maxX = ordered[i].bx;
					setX = true;
				}
				
				if (i + 1 < ordered.length) {
					if (ordered[i + 1].IsXAligned() && maxX == ordered[i + 1].ax &&
						ordered[i].ay == ordered[i + 1].ay) {
						maxX += World.DEFAULT_TILE_SIZE;
						continue;
					}
				}
				
				result.push(new LinePrimitive(minX, ordered[i].ay, maxX, ordered[i].by));
				setX = false;
			}
			
			return result;
		}
		
		/**
		 * 
		 * @param	lines
		 */
		private function CreateXLines(lines:Vector.<LinePrimitive>):void
		{
			var xfilter:b2FilterData = new b2FilterData;
			xfilter.categoryBits = Physic.CATEGORY_SECTOR_X;
			for (var i:uint = 0; i < lines.length; i++)
			{
				var x1:b2Vec2 = new b2Vec2(lines[i].ax, lines[i].ay);
				var x2:b2Vec2 = new b2Vec2(lines[i].bx, lines[i].by);
				var xsh:b2PolygonShape = b2PolygonShape.AsEdge(x1, x2);
				var xfix:b2Fixture = m_body.CreateFixture2(xsh, 1.0);
				xfix.SetFilterData(xfilter);
				xfix.SetFriction(1.0);
			}
		}
		
		/**
		 * 
		 * @param	lines
		 * @param	xnum
		 * @param	ynum
		 * @return
		 */
		private function GenerateYLines(lines:Vector.<LinePrimitive>, xnum:uint, ynum:uint):Vector.<LinePrimitive> {
			var ordered:Vector.<LinePrimitive> = new Vector.<LinePrimitive>;
			for (var tx:uint = 0; tx < xnum; tx++)
			for (var ty:uint = 0; ty < ynum; ty++)
			{
				var n:uint = 0;
				while (n != lines.length) {
					if (lines[n].ax == tx * World.DEFAULT_TILE_SIZE &&
						lines[n].ay == ty * World.DEFAULT_TILE_SIZE &&
						lines[n].IsYAligned()) {
						ordered.push(lines[n]);
						break;
					} n++;
				}
			}
			
			var result:Vector.<LinePrimitive> = new Vector.<LinePrimitive>;
			var setY:Boolean = false;
			var minY:int = 0;
			var maxY:int = 0;
			
			for (var i:uint = 0; i < ordered.length; i++)
			{
				if (!setY) {
					minY = ordered[i].ay;
					maxY = ordered[i].by;
					setY = true;
				}
				
				if (i + 1 < ordered.length) {
					if (ordered[i + 1].IsYAligned() && maxY == ordered[i + 1].ay &&
						ordered[i].ax == ordered[i + 1].ax) {
						maxY += World.DEFAULT_TILE_SIZE;
						continue;
					}
				}
				
				result.push(new LinePrimitive(ordered[i].ax, minY, ordered[i].bx, maxY));
				setY = false;
			}
			
			return result;
		}
		
		/**
		 * 
		 * @param	lines
		 */
		private function CreateYLines(lines:Vector.<LinePrimitive>):void
		{
			var yfilter:b2FilterData = new b2FilterData;
			yfilter.categoryBits = Physic.CATEGORY_SECTOR_Y;
			for (var j:uint = 0; j < lines.length; j++)
			{
				var y1:b2Vec2 = new b2Vec2(lines[j].ax, lines[j].ay);
				var y2:b2Vec2 = new b2Vec2(lines[j].bx, lines[j].by);
				var ysh:b2PolygonShape = b2PolygonShape.AsEdge(y1, y2);
				var yfix:b2Fixture = m_body.CreateFixture2(ysh, 1.0);
				yfix.SetFilterData(yfilter);
				yfix.SetFriction(0.0);
			}
		}
		
		/**
		 * 
		 * @param	lines
		 * @param	xnum
		 * @param	ynum
		 */
		private function GenerateCollision(lines:Vector.<LinePrimitive>, xnum:uint, ynum:uint):void {
			if ((lines = RemoveDuplicateLines(lines)).length != 0)
			{
				var xlines:Vector.<LinePrimitive> = GenerateXLines(lines, xnum, ynum);
				CreateXLines(xlines);
				
				var ylines:Vector.<LinePrimitive> = GenerateYLines(lines, xnum, ynum);
				CreateYLines(ylines);
				
				/*trace("XLINES");
				for (var temp:uint = 0; temp < xlines.length; temp++)
				{
					trace(xlines[temp].ax + ", " + xlines[temp].ay + ", " + xlines[temp].bx + ", " + xlines[temp].by);
				}
				
				trace("YLINES");
				for (var temp:uint = 0; temp < ylines.length; temp++)
				{
					trace(ylines[temp].ax + ", " + ylines[temp].ay + ", " + ylines[temp].bx + ", " + ylines[temp].by);
				}*/
			}
		}
		
		/**
		 * 
		 * @param	info
		 * @return
		 */
		private function BuildTerrain(info:Object):Boolean {
			var tileset:Vector.<TilePattern> = BuildTileset(info.path);
			if (tileset.length != 0)
			{
				var array:Array = info["data"];
				var index:uint = 0;
					
				for (var j:uint = 0; j < (m_numY = info.ynum); j++)
				for (var i:uint = 0; i < (m_numX = info.xnum); i++) {
					if (array[index] != 0)
					{
						for (var k:uint = 0; k < tileset.length; k++) {
							if (array[index] == tileset[k].Indice)
							{
								var tile:TilePattern = tileset[k].Clone();
								tile.x = i * World.DEFAULT_TILE_SIZE;
								tile.y = j * World.DEFAULT_TILE_SIZE;
								
								var p1:Point = new Point(tile.x, tile.y);
								var p2:Point = new Point(tile.x + World.DEFAULT_TILE_SIZE, tile.y);
								var p3:Point = new Point(tile.x + World.DEFAULT_TILE_SIZE, tile.y + World.DEFAULT_TILE_SIZE);
								var p4:Point = new Point(tile.x, tile.y + World.DEFAULT_TILE_SIZE);
								
								m_linelist.push(new LinePrimitive(p1.x, p1.y, p2.x, p2.y));
								m_linelist.push(new LinePrimitive(p2.x, p2.y, p3.x, p3.y));
								m_linelist.push(new LinePrimitive(p4.x, p4.y, p3.x, p3.y));
								m_linelist.push(new LinePrimitive(p1.x, p1.y, p4.x, p4.y));
								m_tilelist.push(addChild(tile));
							}
						}
					}
					
					index++;
				}
				
				if (info.gcol == true) {
					var xcol:Array = info["xcol"];
					var xline:Vector.<LinePrimitive> = new Vector.<LinePrimitive>;
					for (var xit:uint = 0; xit < xcol.length; xit += 4)
					{
						xline.push(new LinePrimitive(xcol[xit + 0], xcol[xit + 1], xcol[xit + 2], xcol[xit + 3]));
					} CreateXLines(xline);
					
					var ycol:Array = info["ycol"];
					var yline:Vector.<LinePrimitive> = new Vector.<LinePrimitive>;
					for (var yit:uint = 0; yit < ycol.length; yit += 4)
					{
						yline.push(new LinePrimitive(ycol[yit + 0], ycol[yit + 1], ycol[yit + 2], ycol[yit + 3]));
					} CreateYLines(yline);
					m_builded = true;
				}
				
				return true;
			}
			
			return false;
		}
		
		/**
		 * 
		 * @param	entity
		 * @param	focus
		 */
		public function AddEntity(entity:IEntity, focus:Boolean = false):void {
			m_entities.push(addChild(entity));
		}
		
		/**
		 * 
		 * @param	entity
		 * @return
		 */
		public function IsEntityInside(entity:IEntity):Boolean {
			if (entity.x >= AbsX && entity.x <= AbsX + (NumX * World.DEFAULT_TILE_SIZE) &&
				entity.y >= AbsY && entity.y <= AbsY + (NumY * World.DEFAULT_TILE_SIZE)) {
				return true;
			}
			
			return false;
		}
		
		/**
		 * 
		 * @param	list
		 */
		public function CheckContacts(list:b2Contact):void {
			for (var c:b2Contact = list; c; c = c.GetNext())
			{
				for (var i:uint = 0; i < m_entities.length; i++) {
					if (m_entities[i].Type == IEntity.PHYSIC_ENTITY ||
						m_entities[i].Type == IEntity.PLAYER_ENTITY ||
						m_entities[i].Type == IEntity.TURTLE_ENTITY) {
							(m_entities[i] as Physic).CheckContact(c);
					}
				}
			}
		}
		
		/**
		 * 
		 * @param	e
		 */
		public function Update(e:Event):void {
			for (var i:uint = 0; i < m_entities.length; i++) {
				m_entities[i].Update(e);
			}
		}
		
		/**
		 * 
		 */
		public function Reset():void
		{
			if (!m_builded) {
				GenerateCollision(m_linelist, m_numX, m_numY);
				m_builded = true;
			}
			
			for (var i:uint = 0; i < m_entities.length; i++) {
				m_entities[i].Reset();
			}
		}
		
		/*/////////////////////////////////////////////////
		/// ACCESSORS:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	name
		 * @return
		 */
		public function GetEntityFromName(name:String):IEntity {
			for (var i:uint = 0; i < m_entities.length; i++) {
				if (m_entities[i].Name == name) {
					return m_entities[i];
				}
			}
			
			return null;
		}
		
		/**
		 * 
		 */
		public function get AbsX():int {
			return m_absX;
		}
		
		/**
		 * 
		 */
		public function get AbsY():int {
			return m_absY;
		}
		
		/**
		 * 
		 */
		public function get NumX():int {
			return m_numX;
		}
		
		/**
		 * 
		 */
		public function get NumY():int {
			return m_numY;
		}
	}
}