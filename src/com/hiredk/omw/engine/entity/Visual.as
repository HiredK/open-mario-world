package com.hiredk.omw.engine.entity 
{
	import flash.events.Event;
	
	import flash.display.BitmapData;
	import flash.display.Bitmap;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import flash.geom.Rectangle;
	import flash.geom.Point;
	
	// engine imports
	import com.deadreckoned.assetmanager.AssetManager;
	import com.hiredk.omw.engine.utility.SpriteSheet;
	import com.hiredk.omw.engine.Sector;
	
	// JSON imports (as3corlib.swc)
	import com.adobe.serialization.json.JSON;
	
	/**
	 * @author HiredK
	 */
	public class Visual extends IEntity {
		/*/////////////////////////////////////////////////
		/// TYPEDEFS/ENUMS:
		/////////////////////////////////////////////////*/
		public static const POSITIVE_SIDE:uint = 0;
		public static const NEGATIVE_SIDE:uint = 1;
		
		/*/////////////////////////////////////////////////
		/// MEMBERS:
		/////////////////////////////////////////////////*/
		private var m_spritelist:Vector.<SpriteSheet>;
		private var m_blocked:Boolean;
		private var m_xoffset:int;
		private var m_yoffset:int;
		private var m_timer:Timer;
		
		// animation members
		protected var m_currentName:String;
		protected var m_currentSide:uint;
		protected var m_currentStep:uint;
		protected var m_current:Bitmap;
		
		/*/////////////////////////////////////////////////
		/// CONSTRUCTOR:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	parent
		 * @param	name
		 * @param	x
		 * @param	y
		 */
		public function Visual(parent:Sector, name:String, x:int, y:int)
		{
			m_spritelist = new Vector.<SpriteSheet>;
			(m_timer = new Timer(0)).addEventListener(TimerEvent.TIMER, OnAnimate);
			m_currentSide = POSITIVE_SIDE;
			super(parent, name, x, y);
		}
		
		/*/////////////////////////////////////////////////
		/// SERVICES:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	path
		 * @return
		 */
		public function BuildSpriteList(path:String):Boolean
		{
			if (AssetManager.getInstance().contains(path)) {
				var info:Object = JSON.decode(AssetManager.getInstance().get(path).asset);
				if (info)
				{
					if (AssetManager.getInstance().contains(info.path)) {
						var source:BitmapData = AssetManager.getInstance().get(info.path).asset;
						if (source)
						{
							var array:Array = info["sprites"];
							for (var i:uint = 0; i < array.length; i++)
							{
								var step:Array = array[i]["step"];
								var data:BitmapData = new BitmapData(array[i].wpxl * step.length, array[i].hpxl, true);
								for (var j:uint = 0; j < step.length; j++)
								{
									var srect:Rectangle = new Rectangle(0, 0, array[i].wpxl, array[i].hpxl);
									srect.x = step[j].xpos;
									srect.y = step[j].ypos;
									
									data.copyPixels(source, srect, new Point(j * array[i].wpxl));
								}
								
								var sprite:SpriteSheet = new SpriteSheet(array[i].name, data);
								sprite.PixelW = array[i].wpxl;
								sprite.PixelH = array[i].hpxl;
								sprite.Freq = array[i].freq;
								sprite.Step = step.length;
								m_spritelist.push(sprite);
							} return true;
						}
					}
				}
			}
			
			return false;
		}
		
		/**
		 * 
		 * @param	name
		 * @param	side
		 */
		public function SetAnimation(name:String, side:uint = POSITIVE_SIDE):void {
			if (m_currentName != name || side != m_currentSide)
			{
				for (var i:uint = 0; i < m_spritelist.length; i++) {
					if (m_spritelist[i].Name == name)
					{
						if (m_current) {
							removeChild(m_current);
							m_current = null;
						}
						
						var data:BitmapData = new BitmapData(m_spritelist[i].PixelW, m_spritelist[i].PixelH, true);
						m_spritelist[i].BlitTo(data, 0, side);
						m_current = new Bitmap(data);
						
						if (m_spritelist[i].Step > 1) {
							m_timer.delay = m_spritelist[i].Freq;
							m_timer.reset();
							m_timer.start();
						}
						else
						{
							m_timer.stop();
						}
						
						m_current.x += m_xoffset;
						m_current.y += m_yoffset;	
						
						m_currentName = name;
						m_currentSide = side;
						m_currentStep = 0;
							
						addChild(m_current);
					}
				}
			}
		}
		
		/**
		 * 
		 * @param	e
		 */
		private function OnAnimate(e:TimerEvent):void {
			for (var i:uint = 0; i < m_spritelist.length; i++) {
				if (m_spritelist[i].Name == m_currentName)
				{
					m_spritelist[i].BlitTo(m_current.bitmapData, m_currentStep++, m_currentSide);
					if (m_currentStep > m_spritelist[i].Step) {
						m_currentStep = 0;
					}
				}
			}
		}
		
		/**
		 * 
		 * @param	e
		 */
		override public function Update(e:Event):void
		{
			super.Update(e);
		}
		
		/**
		 * 
		 */
		override public function Reset():void
		{
			m_currentStep = 0;
			super.Reset();
		}
		
		/*/////////////////////////////////////////////////
		/// ACCESSORS:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 */
		public function set OffsetX(value:int):void {
			m_xoffset = value;
		}
		public function get OffsetX():int {
			return m_xoffset;
		}
		
		/**
		 * 
		 */
		public function set OffsetY(value:int):void {
			m_yoffset = value;
		}
		public function get OffsetY():int {
			return m_yoffset;
		}
		
		/**
		 * 
		 */
		override public function get Type():uint {
			return VISUAL_ENTITY;
		}
	}
}