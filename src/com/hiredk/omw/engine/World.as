package com.hiredk.omw.engine 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.media.Camera;
	
	// engine imports
	import com.deadreckoned.assetmanager.AssetManager;
	import com.hiredk.omw.engine.camera.TCamera;
	import com.hiredk.omw.engine.entity.IEntity;
	
	// JSON imports (as3corlib.swc)
	import com.adobe.serialization.json.JSON;
	
	// Box2D imports
	import Box2D.Dynamics.*;
	import Box2D.Collision.*;
	import Box2D.Collision.Shapes.*;
	import Box2D.Common.Math.*;
	
	/**
	 * @author HiredK
	 */
	public class World extends Sector {
		/*/////////////////////////////////////////////////
		/// TYPEDEFS/ENUMS:
		/////////////////////////////////////////////////*/
		public static const DEFAULT_TILE_SIZE:uint = 16;
		public static const DEFAULT_VIEW_ZOOM:uint = 2;
		
		/*/////////////////////////////////////////////////
		/// MEMBERS:
		/////////////////////////////////////////////////*/
		private var m_groups:Vector.<Sector>;
		private var m_camera:TCamera;
		
		// Box2D members
		private static var m_dynamicWorld:b2World;
		private var m_b2VelocityIteration:uint;
		private var m_b2PositionIteration:uint;
		
		/*/////////////////////////////////////////////////
		/// CONSTRUCTOR:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 */
		public function World() {
			if (m_dynamicWorld == null) {
				m_dynamicWorld = new b2World(new b2Vec2(0.0, 9.8), true);
				m_b2VelocityIteration = 8;
				m_b2PositionIteration = 4;
			}
			
			m_groups = new Vector.<Sector>;
			m_camera = new TCamera(0, 0, 1024, 600);
			this.scaleX = DEFAULT_VIEW_ZOOM;
			this.scaleY = DEFAULT_VIEW_ZOOM;
			super("", 0, 0);
		}
		
		/*/////////////////////////////////////////////////
		/// SERVICES:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	path
		 * @return
		 */
		public function Read(path:String):Boolean {
			if (AssetManager.getInstance().contains(path)) {
				var info:Object = JSON.decode(AssetManager.getInstance().get(path).asset);
				if (info)
				{
					var sectors:Array = info["sectors"];
					for (var i:uint = 0; i < sectors.length; i++)
					{
						const x:int = sectors[i].xpos;
						const y:int = sectors[i].ypos;
						m_groups.push(new Sector(sectors[i].path, x, y));
					} return true;
				}
			}
			
			return false;
		}
		
		/**
		 * 
		 * @param	entity
		 * @param	focus
		 */
		override public function AddEntity(entity:IEntity, focus:Boolean = false):void {
			super.AddEntity(entity, focus);
			if (focus)
			{
				m_camera.SetTarget(entity);
			}
		}
		
		/**
		 * 
		 * @param	e
		 */
		override public function Update(e:Event):void {
			m_dynamicWorld.Step(1.0 / 30.0, m_b2VelocityIteration, m_b2PositionIteration);
			m_dynamicWorld.Step(1.0 / 30.0, m_b2VelocityIteration, m_b2PositionIteration);
			m_dynamicWorld.Step(1.0 / 30.0, m_b2VelocityIteration, m_b2PositionIteration);
			m_dynamicWorld.Step(1.0 / 30.0, m_b2VelocityIteration, m_b2PositionIteration);
			m_dynamicWorld.ClearForces();
			
			for (var i:uint = 0; i < m_groups.length; i++) {
				m_groups[i].CheckContacts(m_dynamicWorld.GetContactList());
				m_groups[i].Update(e);
			}
			
			CheckContacts(m_dynamicWorld.GetContactList());
			this.x = -m_camera.LimitX;
			this.y = -m_camera.LimitY;
			m_camera.Update(e);
			super.Update(e);
		}
		
		/*/////////////////////////////////////////////////
		/// ACCESSORS:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @return
		 */
		public static function GetDynamicWorld():b2World {
			return m_dynamicWorld;
		}
		
		/**
		 * 
		 */
		public function get Groups():Vector.<Sector> {
			return m_groups;
		}
	}
}