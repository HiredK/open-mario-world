package com.hiredk.omw.engine.entity 
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	// engine imports
	import com.hiredk.omw.engine.Sector;
	
	/**
	 * @author HiredK
	 */
	public class IEntity extends Sprite {
		/*/////////////////////////////////////////////////
		/// TYPEDEFS/ENUMS:
		/////////////////////////////////////////////////*/
		public static const VISUAL_ENTITY:uint = 1;
		public static const PHYSIC_ENTITY:uint = 2;
		public static const PLAYER_ENTITY:uint = 3;
		public static const TURTLE_ENTITY:uint = 4;
		
		/*/////////////////////////////////////////////////
		/// MEMBERS:
		/////////////////////////////////////////////////*/
		protected var m_sector:Sector;	
		protected var m_originX:int;
		protected var m_originY:int;
		protected var m_name:String;
		
		/*/////////////////////////////////////////////////
		/// CONSTRUCTOR:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	parent
		 * @param	name
		 * @param	x
		 * @param	y
		 */
		public function IEntity(parent:Sector, name:String, x:int, y:int)
		{
			m_sector = parent;
			this.x = (m_originX=x);
			this.y = (m_originY=y);
			m_name = name;
		}
		
		/*/////////////////////////////////////////////////
		/// SERVICES:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	e
		 */
		public function Update(e:Event):void
		{
		}
		
		/**
		 * 
		 */
		public function Reset():void
		{
		}
		
		/*/////////////////////////////////////////////////
		/// ACCESSORS:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 */
		public function set CurrentSector(value:Sector):void {
			m_sector = value;
		}
		public function get CurrentSector():Sector {
			return m_sector;
		}
		
		/**
		 * 
		 */
		public function get Name():String {
			return m_name;
		}
		
		/**
		 * 
		 */
		public function get Type():uint {
			return 0;
		}
	}
}