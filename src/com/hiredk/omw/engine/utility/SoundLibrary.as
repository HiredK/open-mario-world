package com.hiredk.omw.engine.utility 
{
	import flash.media.Sound;
	import flash.media.SoundChannel;
	
	// engine imports
	import com.deadreckoned.assetmanager.AssetManager;
	
	/**
	 * @author HiredK
	 */
	public class SoundLibrary  {
		/*/////////////////////////////////////////////////
		/// TYPEDEFS/ENUMS:
		/////////////////////////////////////////////////*/
		public static const AUDIO0_REF:uint = 0;
		public static const AUDIO1_REF:uint = 1;
		public static const AUDIO2_REF:uint = 2;
		public static const AUDIO3_REF:uint = 3;
		
		/*/////////////////////////////////////////////////
		/// MEMBERS:
		/////////////////////////////////////////////////*/
		private static var m_sounds:Vector.<Sound>;
		private static var m_channel1:SoundChannel;
		private static var m_channel2:SoundChannel;
		private static var m_enable:Boolean;
		
		/*/////////////////////////////////////////////////
		/// CONSTRUCTOR:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 */
		public function SoundLibrary() {
			m_sounds = new Vector.<Sound>;
			m_channel1 = new SoundChannel;
			m_channel2 = new SoundChannel;
			
			AddSound("sounds/jump.mp3");  // AUDIO0_REF
			AddSound("sounds/hurt.mp3");  // AUDIO1_REF
			AddSound("sounds/stomp.mp3"); // AUDIO2_REF
			AddSound("sounds/overw.mp3"); // AUDIO3_REF
			m_enable = true;
		}
		
		/*/////////////////////////////////////////////////
		/// SERVICES:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	index
		 */
		public static function Play(index:uint, loops:uint = 0):void {
			if (index < m_sounds.length && m_enable)
			{
				if (index == AUDIO3_REF) {
					m_channel1 = m_sounds[index].play(0, loops);
				}
				else
					m_channel2 = m_sounds[index].play(0);
			}
		}
		
		/**
		 * 
		 * @param	path
		 * @return
		 */
		private function AddSound(path:String):Boolean {
			if (AssetManager.getInstance().contains(path)) {
				var sound:Sound = AssetManager.getInstance().get(path).asset as Sound;
				m_sounds.push(sound);
				return true;
			}
			
			return false;
		}

		/**
		 * 
		 * @return
		 */
		public static function Toggle():void {
			m_enable = !m_enable;
			m_channel1.stop();
		}
		
		/**
		 * 
		 * @return
		 */
		public static function IsEnabled():Boolean {
			return m_enable;
		}
	}
}