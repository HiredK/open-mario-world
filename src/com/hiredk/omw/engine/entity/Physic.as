package com.hiredk.omw.engine.entity 
{
	import flash.events.Event;
	import flash.geom.Point;
	
	// engine imports
	import com.deadreckoned.assetmanager.AssetManager;
	import com.hiredk.omw.engine.Sector;
	import com.hiredk.omw.engine.World;
	
	// JSON imports (as3corlib.swc)
	import com.adobe.serialization.json.JSON;
	
	// Box2D imports
	import Box2D.Dynamics.*;
	import Box2D.Collision.*;
	import Box2D.Collision.Shapes.*;
	import Box2D.Common.Math.*;
	
	import Box2D.Dynamics.Contacts.b2Contact;
	
	/**
	 * @author HiredK
	 */
	public class Physic extends Visual {
		/*/////////////////////////////////////////////////
		/// TYPEDEFS/ENUMS:
		/////////////////////////////////////////////////*/
		public static const CATEGORY_SECTOR_X:uint = 1;
		public static const CATEGORY_SECTOR_Y:uint = 2;
		public static const CATEGORY_PLAYER_T:uint = 3;
		public static const CATEGORY_PLAYER_B:uint = 4;
		public static const CATEGORY_TURTLE_T:uint = 5;
		public static const CATEGORY_TURTLE_B:uint = 6;
		
		/*/////////////////////////////////////////////////
		/// MEMBERS:
		/////////////////////////////////////////////////*/
		protected var m_body:b2Body;
		
		/*/////////////////////////////////////////////////
		/// CONSTRUCTOR:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	parent
		 * @param	name
		 * @param	x
		 * @param	y
		 */
		public function Physic(parent:Sector, name:String, x:int, y:int)
		{	
			var bd:b2BodyDef = new b2BodyDef;
			bd.type = b2Body.b2_dynamicBody;
			bd.position.x = parent.x + x;
			bd.position.y = parent.y + y;
			
			m_body = World.GetDynamicWorld().CreateBody(bd);
			super(parent, name, x, y);		
			m_originX = parent.x + x;
			m_originY = parent.y + y;
		}
		
		/*/////////////////////////////////////////////////
		/// SERVICES:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	x
		 * @param	y
		 * @param	w
		 * @param	h
		 * @param	density
		 * @return
		 */
		public function AddSquareFixture(x:int, y:int, w:int, h:uint, density:Number):b2Fixture
		{
			const halfW:uint = w * 0.5;
			const halfH:uint = h * 0.5;
			var bs:b2PolygonShape = b2PolygonShape.AsOrientedBox(x + halfW, y + halfH, new b2Vec2(halfW, halfH));
			return m_body.CreateFixture2(bs, density);
		}
		
		/**
		 * 
		 * @param	x
		 * @param	y
		 * @param	radius
		 * @param	density
		 * @return
		 */
		public function AddCircleFixture(x:int, y:int, radius:int, density:Number):b2Fixture
		{
			var cs:b2CircleShape = new b2CircleShape(radius);
			cs.GetLocalPosition().x = x + radius;
			cs.GetLocalPosition().y = y + radius;
			
			return m_body.CreateFixture2(cs, density);
		}
		
		/**
		 * 
		 * @param	contact
		 */
		public function CheckContact(contact:b2Contact):void
		{
		}
		
		/**
		 * 
		 * @param	e
		 */
		override public function Update(e:Event):void
		{
			if (m_body) {
				this.rotation = m_body.GetAngle() * 180.0 / Math.PI;
				if (Type != PLAYER_ENTITY)
				{
					x = m_body.GetPosition().x - m_sector.AbsX;
					y = m_body.GetPosition().y - m_sector.AbsY;
				}
				else
				{
					x = m_body.GetPosition().x;
					y = m_body.GetPosition().y;
				}
			}
			
			super.Update(e);
		}
		
		/**
		 * 
		 */
		override public function Reset():void {
			var origin:b2Vec2 = new b2Vec2;
			origin.x = m_originX;
			origin.y = m_originY;
			
			m_body.SetPosition(origin);
			super.Reset();
		}
		
		/*/////////////////////////////////////////////////
		/// ACCESSORS:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 */
		override public function get Type():uint {
			return PHYSIC_ENTITY;
		}
	}
}