package com.hiredk.omw.engine.entity 
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;	
	import flash.ui.Keyboard;
	
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	
	// engine imports
	import com.deadreckoned.assetmanager.AssetManager;
	import com.hiredk.omw.engine.utility.SoundLibrary;
	import com.hiredk.omw.engine.Sector;
	
	// JSON imports (as3corlib.swc)
	import com.adobe.serialization.json.JSON;
	
	// Box2D imports
	import Box2D.Dynamics.*;
	import Box2D.Collision.*;
	import Box2D.Collision.Shapes.*;
	import Box2D.Common.Math.*;
	
	import Box2D.Dynamics.Contacts.b2Contact;
	
	/**
	 * @author HiredK
	 */
	public class Player extends Sensor {
		/*/////////////////////////////////////////////////
		/// TYPEDEFS/ENUMS:
		/////////////////////////////////////////////////*/
		public static const STOPPED_R_STATE:uint = 0;
		public static const STOPPED_L_STATE:uint = 1;	
		
		public static const WALKING_R_STATE:uint = 2;
		public static const WALKING_L_STATE:uint = 3;
		
		public static const RUNNING_R_STATE:uint = 4;
		public static const RUNNING_L_STATE:uint = 5;
		
		public static const SLIDING_R_STATE:uint = 6;
		public static const SLIDING_L_STATE:uint = 7;
		
		public static const JUMPUPP_R_STATE:uint = 8;
		public static const JUMPUPP_L_STATE:uint = 9;
		
		public static const JUMPLOW_R_STATE:uint = 10;
		public static const JUMPLOW_L_STATE:uint = 11;
		
		public static const LOOKING_L_STATE:uint = 12;
		public static const LOOKING_R_STATE:uint = 13;
		
		public static const DUCKING_L_STATE:uint = 14;
		public static const DUCKING_R_STATE:uint = 15;
		
		/*/////////////////////////////////////////////////
		/// MEMBERS:
		/////////////////////////////////////////////////*/
		protected var m_currentState:uint;
		
		// player input
		private var m_inputR:Boolean;
		private var m_inputL:Boolean;
		private var m_inputU:Boolean;
		private var m_inputD:Boolean;
		
		// last valid pos
		private var m_lastValidX:Number;
		private var m_lastValidY:Number;
		
		// player health
		private var m_hurting:Boolean;
		private var m_hurtTimer:Timer;
		private var m_hurtStep:uint;
		private var m_health:uint;
		
		/*/////////////////////////////////////////////////
		/// CONSTRUCTOR:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	parent
		 * @param	name
		 * @param	x
		 * @param	y
		 */
		public function Player(parent:Sector, name:String, x:int, y:int)
		{
			m_currentState = STOPPED_R_STATE;
			super(parent, name, x, y);
			m_health = GetMaxHealth();
			m_lastValidX = x;
			m_lastValidY = y;
		}
		
		/*/////////////////////////////////////////////////
		/// SERVICES:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	e
		 */
		public function OnKeyboardDown(e:KeyboardEvent):void {
			if (e.keyCode == Keyboard.SPACE && (m_groundedCheck || m_grounded)) {
				if (m_currentState == STOPPED_R_STATE ||
					m_currentState == WALKING_R_STATE ||
					m_currentState == RUNNING_R_STATE ||
					m_currentState == SLIDING_R_STATE ||
					m_currentState == LOOKING_R_STATE ||
					m_currentState == DUCKING_R_STATE)
				{
					m_currentState = JUMPUPP_R_STATE;
				}
				
				if (m_currentState == STOPPED_L_STATE ||
					m_currentState == WALKING_L_STATE ||
					m_currentState == RUNNING_L_STATE ||
					m_currentState == SLIDING_L_STATE ||
					m_currentState == LOOKING_L_STATE ||
					m_currentState == DUCKING_L_STATE)
				{
					m_currentState = JUMPUPP_L_STATE;
				}
				
				m_body.GetLinearVelocity().y -= 40.0;
				SoundLibrary.Play(SoundLibrary.AUDIO0_REF);
				return;
			}
			
			if (e.keyCode == Keyboard.RIGHT) {
				m_inputR = true;
			}
			if (e.keyCode == Keyboard.LEFT) {
				m_inputL = true;
			}
			if (e.keyCode == Keyboard.UP) {
				m_inputU = true;
			}
			if (e.keyCode == Keyboard.DOWN) {
				m_inputD = true;
			}
		}
		
		/**
		 * 
		 * @param	e
		 */
		public function OnKeyboardUp(e:KeyboardEvent):void
		{
			if (e.keyCode == Keyboard.RIGHT) {
				m_inputR = false;
			}
			if (e.keyCode == Keyboard.LEFT) {
				m_inputL = false;
			}
			if (e.keyCode == Keyboard.UP) {
				m_inputU = false;
			}
			if (e.keyCode == Keyboard.DOWN) {
				m_inputD = false;
			}
		}
		
		/**
		 * 
		 * @param	contact
		 */
		override public function CheckContact(contact:b2Contact):void {
			if (contact.GetFixtureA().GetFilterData().categoryBits == Physic.CATEGORY_SECTOR_X ||
				contact.GetFixtureB().GetFilterData().categoryBits == Physic.CATEGORY_SECTOR_Y) {
				if ((contact.GetFixtureA() == m_sensorFixture ||
					 contact.GetFixtureB() == m_sensorFixture) &&
					 contact.IsTouching())
				{
					if(m_body.GetLinearVelocity().y == 0.0) {
						m_lastValidX = m_body.GetTransform().position.x;
						m_lastValidY = m_body.GetTransform().position.y;
					}
				}
			}
			
			if (contact.GetFixtureA().GetFilterData().categoryBits == Physic.CATEGORY_TURTLE_T ||
				contact.GetFixtureB().GetFilterData().categoryBits == Physic.CATEGORY_TURTLE_T ||
				contact.GetFixtureA().GetFilterData().categoryBits == Physic.CATEGORY_TURTLE_B ||
				contact.GetFixtureB().GetFilterData().categoryBits == Physic.CATEGORY_TURTLE_B) {
				var manifold:b2WorldManifold = new b2WorldManifold;
				contact.GetWorldManifold(manifold);
				if (manifold)
				{
					const deltaX:Number = m_body.GetWorldCenter().x - manifold.m_points[0].x;
					const deltaY:Number = m_body.GetWorldCenter().y - manifold.m_points[0].y;				
					if ((contact.GetFixtureA() == m_physicFixture ||
						 contact.GetFixtureB() == m_physicFixture) &&
						 contact.IsTouching())
					{
						m_body.GetLinearVelocity().x = deltaX * 2.5;
						SoundLibrary.Play(SoundLibrary.AUDIO1_REF);
						Hurt();
						return;
					}
					
					if ((contact.GetFixtureA() == m_sensorFixture ||
						 contact.GetFixtureB() == m_sensorFixture) &&
						 contact.IsTouching())
					{
						if (deltaY < -15.0) {
							m_body.GetLinearVelocity().y -= 25.0;
							SoundLibrary.Play(SoundLibrary.AUDIO2_REF);
						} return;
					}
				}
			}
			
			super.CheckContact(contact);
		}
		
		/**
		 * 
		 */
		public function Fall():void {
			var last:b2Vec2 = new b2Vec2;
			last.x = m_lastValidX;
			last.y = m_lastValidY;
			
			m_body.SetPosition(last);
			Hurt();
		}
		
		/**
		 * 
		 */
		public function Hurt():void
		{
			if (!m_hurting) {
				(m_hurtTimer = new Timer(50)).addEventListener(TimerEvent.TIMER, OnHurtDone);
				m_hurtTimer.start();
				m_hurtStep = 0;
				
				m_hurting = true;
				m_health--;
			}
		}
		
		/**
		 * 
		 * @param	e
		 */
		private function OnHurtDone(e:TimerEvent):void
		{
			if (m_hurtStep++ % 2)
			{
				m_current.alpha = 1.0;
				if (m_hurtStep >= 15)
				{
					m_hurtTimer.reset();
					m_hurting = false;
					return;
				}
			}
			else
				m_current.alpha = 0.0;
		}
		
		/**
		 * 
		 * @param	e
		 */
		override public function Update(e:Event):void {
			switch(m_currentState)
			{
				/* STOPPED STATE */
				case Player.STOPPED_R_STATE: {
					SetAnimation("mario_tall_idle", Visual.POSITIVE_SIDE);
				} break;
				case Player.STOPPED_L_STATE: {
					SetAnimation("mario_tall_idle", Visual.NEGATIVE_SIDE);
				} break;
					
				/* WALKING STATE */
				case Player.WALKING_R_STATE: {
					SetAnimation("mario_tall_walk", Visual.POSITIVE_SIDE);
				} break;
				case Player.WALKING_L_STATE: {
					SetAnimation("mario_tall_walk", Visual.NEGATIVE_SIDE);
				} break;
					
				/* SLIDING STATE */
				case Player.SLIDING_R_STATE: {
					SetAnimation("mario_tall_slide", Visual.POSITIVE_SIDE);
				} break;
				case Player.SLIDING_L_STATE: {
					SetAnimation("mario_tall_slide", Visual.NEGATIVE_SIDE);
				} break;
					
				/* JUMPING STATE */
				case Player.JUMPUPP_R_STATE: {
					SetAnimation("mario_tall_jump_upper", Visual.POSITIVE_SIDE);
				} break;
				case Player.JUMPUPP_L_STATE: {
					SetAnimation("mario_tall_jump_upper", Visual.NEGATIVE_SIDE);
				} break;
				case Player.JUMPLOW_R_STATE: {
					SetAnimation("mario_tall_jump_lower", Visual.POSITIVE_SIDE);
				} break;
				case Player.JUMPLOW_L_STATE: {
					SetAnimation("mario_tall_jump_lower", Visual.NEGATIVE_SIDE);
				} break;
				
				/* LOOKING STATE */
				case Player.LOOKING_R_STATE: {
					SetAnimation("mario_tall_look", Visual.POSITIVE_SIDE);
				} break;
				case Player.LOOKING_L_STATE: {
					SetAnimation("mario_tall_look", Visual.NEGATIVE_SIDE);
				} break;
				
				/* DUCKING STATE */
				case Player.DUCKING_R_STATE: {
					SetAnimation("mario_tall_duck", Visual.POSITIVE_SIDE);
				} break;
				case Player.DUCKING_L_STATE: {
					SetAnimation("mario_tall_duck", Visual.NEGATIVE_SIDE);
				} break;
					
				default:
				{
				} break;
			}
			
			if ((m_inputU || m_inputD) && isTouchingGround()) {
				if (m_currentState == STOPPED_R_STATE ||
					m_currentState == WALKING_R_STATE ||
					m_currentState == RUNNING_R_STATE ||
					m_currentState == SLIDING_R_STATE ||
					m_currentState == JUMPUPP_R_STATE ||
					m_currentState == JUMPLOW_R_STATE)
				{
					if (m_inputU) {
						m_currentState = LOOKING_R_STATE;
					}
					else
					{
						m_currentState = DUCKING_R_STATE;
					}
				}
					
				if (m_currentState == STOPPED_L_STATE ||
					m_currentState == WALKING_L_STATE ||
					m_currentState == RUNNING_L_STATE ||
					m_currentState == SLIDING_L_STATE ||
					m_currentState == JUMPUPP_L_STATE ||
					m_currentState == JUMPLOW_L_STATE)
				{
					if (m_inputU) {
						m_currentState = LOOKING_L_STATE;
					}
					else
						m_currentState = DUCKING_L_STATE;
				}
			}
			else
			{	
				if (m_inputR) {
					m_body.GetLinearVelocity().x += 2.0;
					if (isTouchingGround())
					{
						if (m_body.GetLinearVelocity().x > 0.0) {
							m_currentState = WALKING_R_STATE;
						}
						else
							m_currentState = SLIDING_R_STATE;
					}
					else
						m_currentState = JUMPUPP_R_STATE;
				}
				
				if (m_inputL) {
					m_body.GetLinearVelocity().x -= 2.0;
					if (isTouchingGround())
					{
						if (m_body.GetLinearVelocity().x < 0.0) {
							m_currentState = WALKING_L_STATE;
						}
						else
							m_currentState = SLIDING_L_STATE;
					}
					else
						m_currentState = JUMPUPP_L_STATE;
				}
				
				if (isTouchingGround()) {
					if (Math.abs(m_body.GetLinearVelocity().x) < 0.5 ||
						m_currentState == JUMPLOW_R_STATE ||
						m_currentState == JUMPLOW_L_STATE)
					{
						if (m_currentState == WALKING_R_STATE ||
							m_currentState == RUNNING_R_STATE ||
							m_currentState == SLIDING_R_STATE ||
							m_currentState == JUMPUPP_R_STATE ||
							m_currentState == JUMPLOW_R_STATE ||
							m_currentState == LOOKING_R_STATE ||
							m_currentState == DUCKING_R_STATE)
						{
							m_currentState = STOPPED_R_STATE;
						}
							
						if (m_currentState == WALKING_L_STATE ||
							m_currentState == RUNNING_L_STATE ||
							m_currentState == SLIDING_L_STATE ||
							m_currentState == JUMPUPP_L_STATE ||
							m_currentState == JUMPLOW_L_STATE ||
							m_currentState == LOOKING_L_STATE ||
							m_currentState == DUCKING_L_STATE)
						{
							m_currentState = STOPPED_L_STATE;
						}
					}
				}
				
				if (m_body.GetLinearVelocity().y > 5.0) {
					if (m_currentState == STOPPED_R_STATE ||
						m_currentState == WALKING_R_STATE ||
						m_currentState == RUNNING_R_STATE ||
						m_currentState == SLIDING_R_STATE ||
						m_currentState == JUMPUPP_R_STATE)
					{
						m_currentState = JUMPLOW_R_STATE;
					}
					
					if (m_currentState == STOPPED_L_STATE ||
						m_currentState == WALKING_L_STATE ||
						m_currentState == RUNNING_L_STATE ||
						m_currentState == SLIDING_L_STATE ||
						m_currentState == JUMPUPP_L_STATE)
					{
						m_currentState = JUMPLOW_L_STATE;
					}
				}
			}
			
			m_body.GetLinearVelocity().x *= 0.95;
			super.Update(e);
		}
		
		/*/////////////////////////////////////////////////
		/// ACCESSORS:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 */
		static public function GetMaxHealth():uint {
			return 4;
		}
		
		/**
		 * 
		 */
		public function set Health(value:uint):void {
			m_health = value;
		}
		public function get Health():uint {
			return m_health;
		}
		
		/**
		 * 
		 */
		override public function get Type():uint {
			return PLAYER_ENTITY;
		}
	}
}