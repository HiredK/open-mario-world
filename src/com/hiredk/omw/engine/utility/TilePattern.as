package com.hiredk.omw.engine.utility 
{
	import flash.display.BitmapData;
	import flash.display.Bitmap;
	
	/**
	 * @author HiredK
	 */
	public class TilePattern extends Bitmap {
		/*/////////////////////////////////////////////////
		/// TYPEDEFS/ENUMS:
		/////////////////////////////////////////////////*/
		public static const EMPTY_GROUND:uint = 0;
		public static const BLOCK_GROUND:uint = 1;
		
		/*/////////////////////////////////////////////////
		/// MEMBERS:
		/////////////////////////////////////////////////*/
		private var m_indice:uint;
		private var m_ground:uint;
		
		/*/////////////////////////////////////////////////
		/// CONSTRUCTOR:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	data
		 */
		public function TilePattern(data:BitmapData)
		{
			super(data);
		}
		
		/*/////////////////////////////////////////////////
		/// SERVICES:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @return
		 */
		public function Clone():TilePattern {
			var object:TilePattern = new TilePattern(this.bitmapData);
			object.Indice = Indice;
			object.Ground = Ground;
			return object;
		}
		
		/*/////////////////////////////////////////////////
		/// ACCESSORS:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 */
		public function set Indice(value:uint):void {
			m_indice = value;
		}
		public function get Indice():uint {
			return m_indice;
		}
		
		/**
		 * 
		 */
		public function set Ground(value:uint):void {
			m_ground = value;
		}
		public function get Ground():uint {
			return m_ground;
		}
	}
}