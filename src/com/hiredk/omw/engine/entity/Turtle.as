package com.hiredk.omw.engine.entity 
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;	
	import flash.ui.Keyboard;
	
	// engine imports
	import com.deadreckoned.assetmanager.AssetManager;
	import com.hiredk.omw.engine.Sector;
	
	// JSON imports (as3corlib.swc)
	import com.adobe.serialization.json.JSON;
	
	// Box2D imports
	import Box2D.Dynamics.*;
	import Box2D.Collision.*;
	import Box2D.Collision.Shapes.*;
	import Box2D.Common.Math.*;
	
	import Box2D.Dynamics.Contacts.b2Contact;
	
	/**
	 * @author HiredK
	 */
	public class Turtle extends Sensor {
		/*/////////////////////////////////////////////////
		/// TYPEDEFS/ENUMS:
		/////////////////////////////////////////////////*/
		public static const STOPPED_R_STATE:uint = 0;
		public static const STOPPED_L_STATE:uint = 1;
		
		public static const WALKING_R_STATE:uint = 2;
		public static const WALKING_L_STATE:uint = 3;
		
		/*/////////////////////////////////////////////////
		/// MEMBERS:
		/////////////////////////////////////////////////*/
		protected var m_currentState:uint;
		
		// direction check
		protected var m_directionCheck:Boolean;
		protected var m_direction:Boolean;
		
		/*/////////////////////////////////////////////////
		/// CONSTRUCTOR:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	parent
		 * @param	name
		 * @param	x
		 * @param	y
		 */
		public function Turtle(parent:Sector, name:String, x:int, y:int) {
			m_currentState = STOPPED_R_STATE;
			m_direction = true;
			
			super(parent, name, x, y);
		}
		
		/*/////////////////////////////////////////////////
		/// SERVICES:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	contact
		 */
		override public function CheckContact(contact:b2Contact):void {
			var manifold:b2WorldManifold = new b2WorldManifold;
			contact.GetWorldManifold(manifold);
			if (manifold)
			{
				const deltaX:Number = m_body.GetWorldCenter().x - manifold.m_points[0].x;
				const deltaY:Number = m_body.GetWorldCenter().y - manifold.m_points[0].y;
				if (contact.GetFixtureA().GetFilterData().categoryBits == Physic.CATEGORY_PLAYER_B ||
					contact.GetFixtureB().GetFilterData().categoryBits == Physic.CATEGORY_PLAYER_B) {
					if ((contact.GetFixtureA() == m_physicFixture ||
						 contact.GetFixtureB() == m_physicFixture) &&
						 contact.IsTouching())
					{
						// TURTLE HAS BEEN HIT!
					}
				}
				
				if (Math.abs(m_body.GetLinearVelocity().x) >= 0.1) {
					if (contact.GetFixtureA().GetFilterData().categoryBits == Physic.CATEGORY_SECTOR_Y ||
						contact.GetFixtureB().GetFilterData().categoryBits == Physic.CATEGORY_SECTOR_Y ||
						contact.GetFixtureA().GetFilterData().categoryBits == Physic.CATEGORY_PLAYER_T ||
						contact.GetFixtureB().GetFilterData().categoryBits == Physic.CATEGORY_PLAYER_T ||
						contact.GetFixtureA().GetFilterData().categoryBits == Physic.CATEGORY_PLAYER_B ||
						contact.GetFixtureB().GetFilterData().categoryBits == Physic.CATEGORY_PLAYER_B)
					{
						if ((contact.GetFixtureA() == m_sensorFixture ||
							 contact.GetFixtureB() == m_sensorFixture ||
							 contact.GetFixtureA() == m_physicFixture ||
							 contact.GetFixtureB() == m_physicFixture) &&
							 contact.IsTouching())
						{
							if ((deltaX < 0.0 &&  m_direction) ||
							    (deltaX > 0.0 && !m_direction))
							{
								if (!m_directionCheck) {
									m_direction = !m_direction;
									m_directionCheck = true;
								} return;
							}
						}
					}
				}
				else
				{
					if (!m_directionCheck) {
						m_direction = !m_direction;
						m_directionCheck = true;
					} return;
				}
			}
			
			super.CheckContact(contact);
		}
		
		/**
		 * 
		 * @param	e
		 */
		override public function Update(e:Event):void {
			switch(m_currentState)
			{
				/* STOPPED STATE */
				case Player.STOPPED_R_STATE: {
					SetAnimation("turtle_idle", Visual.POSITIVE_SIDE);
				} break;
				case Player.STOPPED_L_STATE: {
					SetAnimation("turtle_idle", Visual.NEGATIVE_SIDE);
				} break;
				
				/* WALKING STATE */
				case Player.WALKING_R_STATE: {
					SetAnimation("turtle_walk", Visual.POSITIVE_SIDE);
				} break;
				case Player.WALKING_L_STATE: {
					SetAnimation("turtle_walk", Visual.NEGATIVE_SIDE);
				} break;
				
				default:
				{
				} break;
			}
			
			if (m_direction) {
				m_body.GetLinearVelocity().x =  5.0;
				m_currentState = WALKING_R_STATE;
			}
			else
			{
				m_body.GetLinearVelocity().x = -5.0;
				m_currentState = WALKING_L_STATE;
			} m_directionCheck = false;		
			super.Update(e);
		}
		
		/**
		 * 
		 */
		override public function Reset():void {
			m_currentState = STOPPED_R_STATE;
			m_direction = true;
			super.Reset();
		}
		
		/*/////////////////////////////////////////////////
		/// ACCESSORS:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 */
		override public function get Type():uint {
			return TURTLE_ENTITY;
		}
	}
}