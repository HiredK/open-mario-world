package com.hiredk.omw.engine.camera 
{
	import flash.events.Event;
	
	// engine imports
	import com.hiredk.omw.engine.World;
	import com.hiredk.omw.engine.entity.IEntity;
	
	/**
	 * @author HiredK
	 */
	public class TCamera {
		/*/////////////////////////////////////////////////
		/// MEMBERS:
		/////////////////////////////////////////////////*/
		private var m_target:IEntity;
		
		private var m_limitX:Number;
		private var m_limitY:Number;
		private var m_limitW:Number;
		private var m_limitH:Number;
		
		/*/////////////////////////////////////////////////
		/// CONSTRUCTOR:
		/////////////////////////////////////////////////*/
		public function TCamera(x:Number, y:Number, w:Number, h:Number)
		{
			m_limitX = x;
			m_limitY = y;
			m_limitW = w;
			m_limitH = h;
		}
		
		/*/////////////////////////////////////////////////
		/// SERVICES:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	e
		 */
		public function Update(e:Event):void
		{
			if (m_target) {
				m_limitX = (m_target.x + (m_target.width  * 0.5)) * World.DEFAULT_VIEW_ZOOM - m_limitW * 0.5;
				m_limitY = (m_target.y + (m_target.height * 0.5)) * World.DEFAULT_VIEW_ZOOM - m_limitH * 0.5;
				
				if (m_limitX < m_target.CurrentSector.AbsX * World.DEFAULT_VIEW_ZOOM) {
					m_limitX = m_target.CurrentSector.AbsX * World.DEFAULT_VIEW_ZOOM;
				}
				if (m_limitX + m_limitW > (m_target.CurrentSector.AbsX + (m_target.CurrentSector.NumX * World.DEFAULT_TILE_SIZE)) * World.DEFAULT_VIEW_ZOOM) {
					m_limitX = ((m_target.CurrentSector.AbsX + (m_target.CurrentSector.NumX * World.DEFAULT_TILE_SIZE)) * World.DEFAULT_VIEW_ZOOM) - m_limitW;
				}
				
				if (m_limitY < m_target.CurrentSector.AbsY * World.DEFAULT_VIEW_ZOOM) {
					m_limitY = m_target.CurrentSector.AbsY * World.DEFAULT_VIEW_ZOOM;
				}			
				if (m_limitY + m_limitH > (m_target.CurrentSector.AbsY + (m_target.CurrentSector.NumY * World.DEFAULT_TILE_SIZE)) * World.DEFAULT_VIEW_ZOOM) {
					m_limitY = ((m_target.CurrentSector.AbsY + (m_target.CurrentSector.NumY * World.DEFAULT_TILE_SIZE)) * World.DEFAULT_VIEW_ZOOM) - m_limitH;
				}
			}
		}
		
		/*/////////////////////////////////////////////////
		/// ACCESSORS:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	entity
		 */
		public function SetTarget(entity:IEntity):void {
			m_target = entity;
		}
		
		/**
		 * 
		 */
		public function get LimitX():Number {
			return m_limitX;
		}
		
		/**
		 * 
		 */
		public function get LimitY():Number {
			return m_limitY;
		}
	}
}