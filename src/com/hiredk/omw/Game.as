package com.hiredk.omw
{
	import com.hiredk.omw.engine.utility.SoundLibrary;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	import flash.display.BitmapData;
	import flash.display.Bitmap;
	
	import flash.events.KeyboardEvent;
	import flash.events.Event;
	
	// engine imports
	import com.deadreckoned.assetmanager.AssetManager;
	import com.deadreckoned.assetmanager.events.AssetEvent;
	import com.hiredk.omw.engine.entity.Player;
	import com.hiredk.omw.engine.entity.Physic;
	import com.hiredk.omw.engine.World;
	
	/**
	 * @author HiredK
	 */
	public class Game extends Sprite {
		/*/////////////////////////////////////////////////
		/// TYPEDEFS/ENUMS:
		/////////////////////////////////////////////////*/
		public static const START_SCREEN:uint = 1;
		public static const WORLD_SCREEN:uint = 2;
		public static const GOVER_SCREEN:uint = 3;
		
		/*/////////////////////////////////////////////////
		/// MEMBERS:
		/////////////////////////////////////////////////*/
		private var m_currentScreen:uint = START_SCREEN;
		private var m_overlays:Vector.<Bitmap>;
		private var m_sounds:SoundLibrary;
		private var m_world:World;
		
		/*/////////////////////////////////////////////////
		/// CONSTRUCTOR:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 */
		public function Game() {
			m_overlays = new Vector.<Bitmap>;
			AssetManager.getInstance().addEventListener(AssetEvent.QUEUE_COMPLETE, OnQueueComplete);
			AssetManager.getInstance().path = "data/";
			
			// preload assets specifed in a xml file
			AssetManager.getInstance().loadFromXML("asset-list.xml");
			AssetManager.verbose = true;
		}
		
		/*/////////////////////////////////////////////////
		/// SERVICES:
		/////////////////////////////////////////////////*/
		/**
		 * 
		 * @param	e
		 */
		private function OnQueueComplete(e:AssetEvent):void {
			if ((m_world = new World).Read("world.json"))
			{
				if (m_sounds == null) {
					m_sounds = new SoundLibrary;
				}
				
				var player:Player = new Player(m_world, "mario", 180, 600);
				if (player.BuildSpriteList("sprites/mario.json")) {
					player.BuildSensor(14, 20, Physic.CATEGORY_PLAYER_T, Physic.CATEGORY_PLAYER_B);
					player.OffsetX = -9;
					player.OffsetY = -2;
					
					stage.addEventListener(KeyboardEvent.KEY_DOWN, player.OnKeyboardDown);
					stage.addEventListener(KeyboardEvent.KEY_UP, player.OnKeyboardUp);
					m_world.AddEntity(player, true);
				}
				
				addEventListener(Event.ENTER_FRAME, Update);
				stage.addEventListener(KeyboardEvent.KEY_DOWN, OnKeyboardDown);
				addChild(m_world);
			}
		}
		
		/**
		 * 
		 * @param	name
		 * @return
		 */
		private function CreateOverlayImage(path:String):Bitmap
		{
			if (AssetManager.getInstance().contains(path)) {
				for (var i:uint = 0; i < m_overlays.length; i++) {
					if (m_overlays[i].name == path) {
						return m_overlays[i];
					}
				}
				
				var source:BitmapData = AssetManager.getInstance().get(path).asset;
				if (source)
				{
					var overlay:Bitmap = new Bitmap(source);
					overlay.name = path;
					
					m_overlays.push(addChild(overlay));
					return overlay;
				}
			}
			
			return null;
		}
		
		/**
		 * 
		 * @param	image
		 */
		private function DeleteOverlayImage(image:Bitmap):void {
			for (var i:uint = 0; i < m_overlays.length; i++) {
				if (m_overlays[i] == image)
				{
					removeChild(m_overlays[i]);
				}
			}
		}
		
		private function ClearOverlayImages():void {
			for (var i:uint = 0; i < m_overlays.length; i++) {
				DeleteOverlayImage(m_overlays[i]);
			}
			
			m_overlays = new Vector.<Bitmap>;
		}
		
		private function OnKeyboardDown(e:KeyboardEvent):void {
			if (m_currentScreen == START_SCREEN) {
				if (e.keyCode == Keyboard.ENTER)
				{
					m_currentScreen = WORLD_SCREEN;
					SoundLibrary.Play(SoundLibrary.AUDIO3_REF,9999);
					ClearOverlayImages();
					return;
				}
			}
		}
		
		private function OnToggleSound(e:MouseEvent):void {
			if (mouseX > 980 && mouseY > 560) // ya I know..
			{
				SoundLibrary.Toggle();
				if (SoundLibrary.IsEnabled()) {
					SoundLibrary.Play(SoundLibrary.AUDIO3_REF,9999);
				}
			}
		}
		
		/**
		 * 
		 * @param	e
		 */
		private function Update(e:Event):void
		{
			switch(m_currentScreen)
			{
				case START_SCREEN: {
					var frame:Bitmap = CreateOverlayImage("sprites/frame.png");
					frame.x = 0.0;
					frame.y = 0.0;
					
					var title:Bitmap = CreateOverlayImage("sprites/title.png");
					title.x = 160.0;
					title.y = 120.0;
				} break;
				
				case WORLD_SCREEN: {
					var player:Player = m_world.GetEntityFromName("mario") as Player;
					if (player)
					{
						var sound:Bitmap = CreateOverlayImage("sprites/sound.png");
						if (!stage.hasEventListener(MouseEvent.CLICK))
						{
							stage.addEventListener(MouseEvent.CLICK, OnToggleSound);
						}
						
						if (SoundLibrary.IsEnabled()) {
							sound.alpha = 1.0;
						}
						else
							sound.alpha = 0.5;
						
						sound.scaleX = 2.0;
						sound.scaleY = 2.0;
						sound.x = 986.0;
						sound.y = 564.0;
						
						var health:Bitmap = null;
						switch(player.Health)
						{
							case 0:
								health = CreateOverlayImage("sprites/health/health0.png");
								m_currentScreen = GOVER_SCREEN;
								break;
								
							case 1:
								health = CreateOverlayImage("sprites/health/health1.png");
								break;
								
							case 2:
								health = CreateOverlayImage("sprites/health/health2.png");
								break;
								
							case 3:
								health = CreateOverlayImage("sprites/health/health3.png");
								break;
								
							case 4:
								health = CreateOverlayImage("sprites/health/health4.png");
								break;
								
							default:
							{
							} break;
						}
						
						if (health) {
							health.scaleX = 2.0;
							health.scaleY = 2.0;
							health.x = 462.0;
							health.y = 16.0;
						}
						
						var outside:Boolean = true;
						for (var i:uint = 0; i < m_world.Groups.length; i++)
						{
							if (m_world.Groups[i].IsEntityInside(player))
							{
								// page sector in
								if (!m_world.contains(m_world.Groups[i])) {
									if (player.CurrentSector != m_world.Groups[i]) {
										m_world.Groups[i].Reset();
									}
									
									m_world.addChildAt((player.CurrentSector = m_world.Groups[i]), 0);
								} outside = false;
							}
							else if (m_world.contains(m_world.Groups[i]))
							{
								// page sector out
								m_world.removeChild(m_world.Groups[i]);
							}
						}
						
						if (outside) {
							player.Fall();
						}
					}
					
					m_world.Update(e);
					break;
				}
				
				case GOVER_SCREEN: {
					if (contains(m_world)) {
						ClearOverlayImages();
						removeChild(m_world);
					}
					
					var gover:Bitmap = CreateOverlayImage("sprites/gover.png");
					gover.x = 0.0;
					gover.y = 0.0;
					
					frame = CreateOverlayImage("sprites/frame.png");
					frame.x = 0.0;
					frame.y = 0.0;
				} break;
				
				default: {
				} break;
			}
		}
	}
}